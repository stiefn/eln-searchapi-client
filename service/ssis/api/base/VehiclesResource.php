<?php
namespace service\ssis\api\base;

use Swagger\Client\Api\VehiclesApi;

/**
 * The VehicleResource extended the Swagger-generated VehiclesApi class
 * This class is intended for future client features of the SSiS
 * 
 * @copyright (c) S&S Internet Systeme GmbH
 * @author Martin Soisch <technik@ssis.de>
 * @since 2018-11-13
 * 
 */
class VehiclesResource extends VehiclesApi {
    //put your code here
}
