<?php

namespace service\ssis\api;

use Exception;
use Swagger\Client\Api\DealersApi;
use Swagger\Client\Api\InquiryApi;
use Swagger\Client\Api\VehiclesApi;
use Swagger\Client\Configuration;
use service\ssis\api\base\DealersResource;
use service\ssis\api\base\VehiclesResource;
use service\ssis\api\base\InquiryResource;

use Swagger\Client\Model\Anfrage;
use Swagger\Client\Model\ChangeInquiryRequest;
use Swagger\Client\Model\CustomerInquiryData;
use Swagger\Client\Model\DirectInquiry;
use Swagger\Client\Model\DirectInquiryDetail;
use Swagger\Client\Model\GetInquiriesRequest;
use Swagger\Client\Model\PaginationParameters;
use Swagger\Client\Model\DealerDetailsRequest;
use Swagger\Client\Model\SearchInquiry;
use Swagger\Client\Model\TagSearch;
use Swagger\Client\Model\VehicleSearch;
use Swagger\Client\Model\VehicleDetailsRequest;
use Swagger\Client\Model\DealerResponse;
use Swagger\Client\Model\TagSearchResult;
use Swagger\Client\Model\VehicleSearchResult;
use Swagger\Client\Model\VehicleResponse;
use Swagger\Client\Model\MetaResponse;

/**
 * Description of Client
 * Bitbucket: https://bitbucket.org/ssinternetsystemegmbh/searchapi-client-php
 * 
 * @copyright (c) S&S Internet Systeme GmbH
 * @author Martin Soisch <technik@ssis.de>
 * @since 2018-10-31
 * 
 */
class Client {

    const AGG_TYPE_MANUFACTURER = 'MANUFACTURER',
            AGG_TYPE_SERIES = 'SERIES',
            AGG_TYPE_ORDERSTATUS = 'ORDERSTATUS',
            AGG_TYPE_BODYTYPE = 'BODYTYPE',
            AGG_TYPE_BODYTYPEGROUP = 'BODYTYPEGROUP',
            AGG_TYPE_FUELTYPEPRIMARY = 'FUELTYPEPRIMARY',
            AGG_TYPE_DEALERID_VEHICLEOWNER = 'DEALERID_VEHICLEOWNER',
            AGG_TYPE_COLORTAG = 'COLORTAG',
            AGG_TYPE_UPHOLSTERYTYPETAG = 'UPHOLSTERYTYPETAG',
            AGG_TYPE_PROPULSIONTYPETAG = 'PROPULSIONTYPETAG',
            AGG_TYPE_POLLUTIONBADGETAG = 'POLLUTIONBADGETAG',
            AGG_TYPE_EURONORMTAG = 'EURONORMTAG',
            AGG_TYPE_TRANSMISSIONTYPE = 'TRANSMISSIONTYPE',
            AGG_TYPE_AGECATEGORYTAG = 'AGECATEGORYTAG';
    
    /**
     *
     * @var string 
     */
    protected $clientVersion = '1.1.197';
    
    /**
     *
     * @var int 
     */
    protected $_dealerId = null;

    /**
     *
     * @var Configuration 
     */
    protected $_configuration = null;

    /**
     *
     * @var DealersApi 
     */
    protected $_dealersApi = null;

    /**
     *
     * @var VehiclesApi 
     */
    private $_vehiclesApi = null;

    /**
     *
     * @var InquiryApi
     */
    private $_inquiryApi = null;

    /**
     * Default pagination parameters
     * 
     * @var array 
     */
    private $_pagParams = ['result_count' => 10, 'result_offset' => 0, 'sort_type' => 'id', 'sort_order_asc' => true];

    /**
     *
     * @var PaginationParameters 
     */
    protected $_paginationParameters = null;

    /**
     * Will be set from dealer or vehicle methods
     * 
     * @var MetaResponse 
     */
    protected $lastMetadata = null;
    
    public function __construct($username = null, $password = null, $dealerId = null, $host = null) {
        $objApiConfig = new ApiConfig();
        
        $config = $objApiConfig->getConfig();
        
        /** Set your dealerId **/
        if ($dealerId !== null) {
            $this->_dealerId = $dealerId;
            $objApiConfig->setDealerId($dealerId);
        } elseif (isset($config['dealerid']) && is_int($config['dealerid'])) {
            $this->_dealerId = $config['dealerid'];
        }
        
        /** Set the configuration */
        $this->_configuration = new Configuration();
        
        $this->_configuration->setUserAgent('SSiS-API PHP Client V1.0');
        $this->_configuration->setTempFolderPath($config['temp_folder_path']);
        $this->_configuration->setDebugFile($config['debug_file']);

        if ($host != null) {
            $this->_configuration->setHost(trim($host));
        } elseif (isset($config['host']) && trim($config['host']) !== '') {
            $this->_configuration->setHost(trim($config['host']));
        }
        
        if ($username !== null && $password !== null) {
            $this->_configuration->setUsername($username);
            $this->_configuration->setPassword($password);
        } else {
            $this->_configuration->setUsername($config['username']);
            $this->_configuration->setPassword($config['password']);
        }

        $this->_configuration->setDebug(FALSE);

        /* init the pagination parameters */
        $this->_paginationParameters = new PaginationParameters($this->_pagParams);

        $this->_dealersApi = new DealersResource(NULL, $this->_configuration);
        $this->_vehiclesApi = new VehiclesResource(NULL, $this->_configuration);
        $this->_inquiryApi = new InquiryResource(NULL, $this->_configuration);
    }
    
    /**
     * Returns the version of the API Client
     * 
     * @return string
     */
    public function getClientVersion() {
        return $this->clientVersion;
    }
    
    /**
     * Returns the API Url
     * 
     * @return string
     */
    public function getHost() {
        return $this->_configuration->getHost();
    }
    
    /**
     * Set the API Url
     * 
     * @param string $host
     * @return Client
     */
    public function setHost($host) {
        $this->_configuration->setHost($host);
        
        return $this;
    }
    
    /**
     * Returns the Dealer ID
     * 
     * @return int
     */
    public function getDealerId() {
        return $this->_dealerId;
    }
    
    /**
     * Sets the Dealer ID
     * 
     * @param int $dealerId
     * @return $this
     */
    public function setDealerId($dealerId) {
        $this->_dealerId = $dealerId;
        return $this;
    }

    /**
     * Returns the Pagination Parameters
     * 
     * @return array
     */
    public function getPagParams() {
        $pagParams = $this->_pagParams;

        return $pagParams;
    }

    /**
     * Set the Pagination Parameters Object
     * 
     * @param array $pagParams
     * @return $this
     */
    public function setPaginationParameters(array $pagParams) {
        $this->_pagParams = $pagParams;
        $this->_paginationParameters = new PaginationParameters($pagParams);

        return $this;
    }

    /**
     * Returns the Pagination Parameters Object
     * 
     * @return PaginationParameters
     */
    public function getPaginationParameters() {
        $paginationParameters = $this->_paginationParameters;

        return $paginationParameters;
    }

    /**
     * Set the number of return values
     * (value in PaginationParameters)
     * 
     * @param int $cnt
     * @return $this
     */
    public function setPagParamResultCount($cnt) {
        $this->_paginationParameters->setResultCount($cnt);

        return $this;
    }

    /**
     * Set the offset of the return values
     * (value in PaginationParameters)
     * 
     * @param int $offset
     * @return $this
     */
    public function setPagParamResultOffset($offset) {
        $this->_paginationParameters->setResultOffset($offset);

        return $this;
    }

    /**
     * Set the fieldname for sort (default: id)
     * (value in PaginationParameters)
     * 
     * @param string $sortType
     * @return $this
     */
    public function setPagParamSortType($sortType) {
        $this->_paginationParameters->setSortType($sortType);

        return $this;
    }

    /**
     * Set the sort direction (default: ascending)
     * (value in PaginationParameters)
     * 
     * @param bool $sortOrderAsc
     * @return $this
     */
    public function setPagParamSortOrderAsc($sortOrderAsc) {
        $this->_paginationParameters->setSortOrderAsc($sortOrderAsc);

        return $this;
    }

    /**
     * Returns a DealerDetailsRequest Object
     * used for getDealerDetails()
     * 
     * @return DealerDetailsRequest
     */
    public function getDealerDetailsRequest() {
        $dealerSearchDetailsRequest = new DealerDetailsRequest();

        return $dealerSearchDetailsRequest;
    }

    /**
     * Returns a TagSearch Object
     * used for getVehicleAggregationTags()
     * 
     * @return TagSearch
     */
    public function getVehicleTagSearch() {
        $tagSearch = new TagSearch();

        return $tagSearch;
    }

    /**
     * Returns a VehicleSearch Object
     * used for getVehicleSearchResult()
     * 
     * @return VehicleSearch
     */
    public function getVehicleSearch() {
        $vehicleSearch = new VehicleSearch();

        return $vehicleSearch;
    }

    /**
     * Returns a VehicleDetailsRequest Object
     * used for getVehicleDetails()
     * 
     * @return VehicleDetailsRequest
     */
    public function getVehicleDetailsRequest() {
        $vehicleDetailsRequest = new VehicleDetailsRequest();

        return $vehicleDetailsRequest;
    }

    /**
     * Returns a DealerResponse Object with data from API
     * 
     * @param DealerDetailsRequest $dealerDetailsRequest
     * @return DealerResponse
     */
    public function getDealerDetails(DealerDetailsRequest $dealerDetailsRequest) {
        $objDealerResponseWithMeta = $this->_dealersApi->getDealerDetails($dealerDetailsRequest);

        $this->lastMetadata = $objDealerResponseWithMeta->getMeta();

        return $objDealerResponseWithMeta->getResponse();
    }

    /**
     * Returns a TagSearchResult Object with data from API
     * 
     * @param TagSearch $vehicleTagSearch
     * @param int $dealerid
     * 
     * @return TagSearchResult
     */
    public function getVehicleAggregationTags(TagSearch $vehicleTagSearch, $dealerid = null) {
        if ($dealerid === null) {
            $dealerid = $this->_dealerId;
        }
        
        $objTagSearchResultWithMeta = $this->_vehiclesApi->getVehicleAggregationTags($dealerid, $vehicleTagSearch);

        $this->lastMetadata = $objTagSearchResultWithMeta->getMeta();

        return $objTagSearchResultWithMeta->getResponse();
    }

    /**
     * Returns a VehicleSearchResult Object with data from API
     * 
     * @param VehicleSearch $vehicleSearch
     * @param int $dealerid
     * 
     * @return VehicleSearchResult
     */
    public function getVehicleSearchResult(VehicleSearch $vehicleSearch, $dealerid = null) {
        if ($dealerid === null) {
            $dealerid = $this->_dealerId;
        }
        
        if ($vehicleSearch->getPaginationParameters() === null) {
            $vehicleSearch->setPaginationParameters($this->_paginationParameters);
        }

        $objVehicleSearchResultWithMeta = $this->_vehiclesApi->searchDealerVehicles($dealerid, $vehicleSearch);

        $this->lastMetadata = $objVehicleSearchResultWithMeta->getMeta();

        return $objVehicleSearchResultWithMeta->getResponse();
    }

    /**
     * Returns a VehicleResponse Object with data from API
     * 
     * @param VehicleDetailsRequest $vehicleDetailsRequest
     * @param int $dealerid
     * 
     * @return VehicleResponse
     */
    public function getVehicleDetails(VehicleDetailsRequest $vehicleDetailsRequest, $dealerid = null) {
        if ($dealerid === null) {
            $dealerid = $this->_dealerId;
        }
        
        $objVehicleResponseWithMeta = $this->_vehiclesApi->getVehicleDetails($dealerid, $vehicleDetailsRequest);

        $this->lastMetadata = $objVehicleResponseWithMeta->getMeta();

        return $objVehicleResponseWithMeta->getResponse();
    }

    /**
     * Used for changeInquiry()
     *
     * @return ChangeInquiryRequest
     */
    public function getChangeInquiryRequest() {
        $changeInquiryRequest = new ChangeInquiryRequest();

        return $changeInquiryRequest;
    }

    /**
     *
     *
     * @param ChangeInquiryRequest $changeInquiryRequest
     * @param int $dealerId
     * @return \Swagger\Client\Model\InquiryDefaultResult
     */
    public function changeInquiry(ChangeInquiryRequest $changeInquiryRequest, $dealerId=null) {
        if ($dealerId === null) {
            $dealerId = $this->_dealerId;
        }

        $defaultResponse = $this->_inquiryApi->changeInquiry($dealerId, $changeInquiryRequest);

        $this->lastMetadata = $defaultResponse->getMeta();

        return $defaultResponse->getResponse();
    }

    /**
     * Required Params:
     * <pre>
     * name
     * firstname
     * street
     * postcode
     * city
     * country
     * telefon_daytime
     * telefon_evening
     * email
     * fax
     * age
     * profession
     * company
     * privacy_protection
     * </pre>
     * @return CustomerInquiryData
     */
    public function getCustomerInquiryData() {
        $customerInquiryData = new CustomerInquiryData();

        return $customerInquiryData;
    }

    /**
     *
     * @return DirectInquiryDetail
     */
    public function getDirectInquiryDetail() {
        $directInquiryDetail = new DirectInquiryDetail();

        return $directInquiryDetail;
    }

    /**
     *
     * @return DirectInquiry
     */
    public function getDirectInquiryRequest() {
        $directInquiry = new DirectInquiry();

        return $directInquiry;
    }

    /**
     *
     *
     * @param DirectInquiry $directInquiry
     * @param int $dealerId
     * @return \Swagger\Client\Model\InquiryDefaultResult
     */
    public function direktinquiry(DirectInquiry $directInquiry, $dealerId=null) {
        if ($dealerId === null) {
            $dealerId = $this->_dealerId;
        }

        $inquiryDefaultResultWithMeta = $this->_inquiryApi->direktinquiry($dealerId, $directInquiry);

        $this->lastMetadata = $inquiryDefaultResultWithMeta->getMeta();

        return $inquiryDefaultResultWithMeta->getResponse();
    }

    /**
     * Used for direktsearch() and freesearch()
     *
     * @return Anfrage
     */
    public function getAnfrage() {
        $objAnfrage = new Anfrage();

        return $objAnfrage;
    }

    /**
     * Used for direktsearch() and freesearch()
     *
     * @return SearchInquiry
     */
    public function getSearchInquiryRequest() {
        $searchInquiry = new SearchInquiry();

        return $searchInquiry;
    }


    /**
     *
     *
     * @param SearchInquiry $searchInquiry
     * @param int $dealerId
     * @return \Swagger\Client\Model\InquiryDefaultResult
     */
    public function direktsearch(SearchInquiry $searchInquiry, $dealerId=null) {
        if ($dealerId === null) {
            $dealerId = $this->_dealerId;
        }

        $inquiryDefaultResultWithMeta = $this->_inquiryApi->direktsearch($dealerId, $searchInquiry);

        $this->lastMetadata = $inquiryDefaultResultWithMeta->getMeta();

        return $inquiryDefaultResultWithMeta->getResponse();
    }

    /**
     *
     * @param SearchInquiry $searchInquiry
     * @return \Swagger\Client\Model\InquiryDefaultResult
     */
    public function freesearch(SearchInquiry $searchInquiry) {
        $inquiryDefaultResultWithMeta = $this->_inquiryApi->freesearch($searchInquiry);

        $this->lastMetadata = $inquiryDefaultResultWithMeta->getMeta();

        return $inquiryDefaultResultWithMeta->getResponse();
    }

    /**
     *
     * @return GetInquiriesRequest
     */
    public function getInquiriesRequest() {
        $getInquiriesRequest = new GetInquiriesRequest();

        return $getInquiriesRequest;
    }

    /**
     *
     * @param GetInquiriesRequest $getInquiriesRequest
     * @param int $dealerId
     * @return \Swagger\Client\Model\GetInquiryResult
     */
    public function getInquiries(GetInquiriesRequest $getInquiriesRequest, $dealerId=null) {
        if ($dealerId === null) {
            $dealerId = $this->_dealerId;
        }

        $getInquiryResultWithMeta = $this->_inquiryApi->getInquiries($dealerId, $getInquiriesRequest);

        $this->lastMetadata = $getInquiryResultWithMeta->getMeta();

        return $getInquiryResultWithMeta->getResponse();
    }

    /**
     * 
     * @return MetaResponse
     * @throws Exception
     */
    public function getLastMetadata() {
        $objMetadata = $this->lastMetadata;

        if ($objMetadata === null) {
            throw new Exception('To get the last metadata, you must run a search first.', 999);
        }
        return $objMetadata;
    }

}
