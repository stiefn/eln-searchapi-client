<?php
require_once '../autoload.php';

use service\ssis\api\Client;
use Swagger\Client\Model\PaginationParameters;

$aryTests = [
    //'DealerDetails',
    //'VehicleAggregationTags',
    //'VehicleAggregationTagsWithCount',
    //'VehicleSearch',
    //'VehicleDetails',
    //'changeInquiry',
    //'direktinquiry',
    //'direktsearch',
    //'freesearch',
    //'getInquiries',
    //'check4duplicates',
    'checkDiffBetweenSearchAndDetails'
    //'Metadata'
];

try {
    // if you are don't use the config.php, you must set your username, password and dealer ID as parameters for the Client class.
    $objClient = new Client();
    
    //$objClient->setPagParamResultCount(5);
    
    echo '<h1>Client Version: ' . $objClient->getClientVersion() . '</h1>';
    
    /** Test Dealer Details **/
    if (in_array('DealerDetails', $aryTests)) {
        $aryDealerIds = [0000]; // your dealerId
        
        $objDealerDetailsRequest = $objClient->getDealerDetailsRequest();
        $objDealerDetailsRequest->setDealerIds($aryDealerIds);
        echo '<h1>Dealer Details</h1>';
        echo '<pre>';
        var_dump($objClient->getDealerDetails($objDealerDetailsRequest));
        echo '</pre>';
    }
    
    /** Test Vehicle Aggregation Tags **/
    if (in_array('VehicleAggregationTags', $aryTests)) {
        $aryAggTypes = [Client::AGG_TYPE_MANUFACTURER];
        
        $objVehicleTagSearch = $objClient->getVehicleTagSearch();
        $objVehicleTagSearch->setAggregationTypes($aryAggTypes);
        echo '<h1>Vehicle Aggregation Tags</h1>';
        echo '<pre>';
        print_r($objClient->getVehicleAggregationTags($objVehicleTagSearch));
        echo '</pre>';
    }
    
    /** Test Vehicle Aggregation Tags with Count **/
    if (in_array('VehicleAggregationTagsWithCount', $aryTests)) {
        $aryAggTypes = [Client::AGG_TYPE_MANUFACTURER];
        
        $objVehicleSearch = $objClient->getVehicleSearch();
        $objVehicleSearch->setAggregationTypes($aryAggTypes);
        
        $objClient->setPagParamResultCount(0);
        
        echo '<h1>Vehicle Aggregation Tags With Count</h1>';
        echo '<pre>';
        print_r($objClient->getVehicleSearchResult($objVehicleSearch));
        echo '</pre>';
    }
    
    /** Test Vehicle Search **/
    if (in_array('VehicleSearch', $aryTests)) {
        $objVehicleSearch = $objClient->getVehicleSearch();
        //$objVehicleSearch->setDealerIdCaller($dealerId);
        //$objVehicleSearch->setDoorCount([2]);
        
        $objClient->setPagParamSortType('date_modified');
        $objClient->setPagParamSortOrderAsc(TRUE);
        
        $objModelSearch = new \Swagger\Client\Model\ModelSearch();
        //$objModelSearch->setManufacturerTag('VOLVO');
        $objVehicleSearch->setModelSearch([$objModelSearch]);
        
        $objClient->setPagParamResultCount(25);
        
        echo '<h1>Vehicle Search</h1>';
        echo '<pre>';
        print_r($objClient->getVehicleSearchResult($objVehicleSearch));
        echo '</pre>';
    }
    
    /** Test Vehicle Details **/
    if (in_array('VehicleDetails', $aryTests)) {
        $objVehicleDetailsRequest = $objClient->getVehicleDetailsRequest();

        $objVehicleDetailsRequest->setVehicleIds(['dadfffe632200c37e9e876bf04657eab']);
                
        echo '<h1>Vehicle Details</h1>';
        echo '<pre>';
        var_dump($objClient->getVehicleDetails($objVehicleDetailsRequest));
        echo '</pre>';
    }

    /** Test changeInquiry **/
    if (in_array('changeInquiry', $aryTests)) {
        $objChangeInquiryRequest = $objClient->getChangeInquiryRequest();
        echo "<h1>Change Inquiry</h1>";
        echo '<pre>';
        $aryResponse = $objClient->changeInquiry($objChangeInquiryRequest);
        print_r($aryResponse);
        echo '</pre>';
    }

    /** Test direktinquiry **/
    //*** Momentan funktioniert hier die API nicht richtig 2018-11-13 ***//
    if (in_array('direktinquiry', $aryTests)) {
        $objCustomerInquiryData = $objClient->getCustomerInquiryData();
        $objCustomerInquiryData->setCountry('DE');

        $objCustomerInquiryData->setFirstname('Martin');
        $objCustomerInquiryData->setName('Soisch');
        $objCustomerInquiryData->setCity('Moenchengladbach');
        $objCustomerInquiryData->setPostcode('41063');
        $objCustomerInquiryData->setStreet('Bayernstr. 7');
        $objCustomerInquiryData->setSalutation('Herr');
        $objCustomerInquiryData->setEmail('msh@ssis.de');
        $objCustomerInquiryData->setTelefonDaytime('02166-8787877');
        //$objCustomerInquiryData->setTelefonEvening('02166-8787877');
        //$objCustomerInquiryData->setFax('-');
        //$objCustomerInquiryData->setAge('-');
        //$objCustomerInquiryData->setCompany('-');
        //$objCustomerInquiryData->setProfession('-');
        $objCustomerInquiryData->setPrivacyProtection(true);

        $objDirectInquiryDetail = $objClient->getDirectInquiryDetail();
        $objDirectInquiryDetail->setVehicleId('3e3c5d35fdd13ab39edffa9bbf86545a'); // fuer DEV
        //$objDirectInquiryDetail->setVehicleId('b7bc23082e0acc0d12e60dac67289546'); // fuer PROD
        $objDirectInquiryDetail->setCargarantie(7);
        $objDirectInquiryDetail->setKaufzeitraum('-');

        /* Hier werden die Equipment IDs uebergeben */
        $objDirectInquiryDetail->setOptionalEquipmentIds(array(22997328527, 22997328524, 22997328311, 22997328341, 22997328356, 22997328287, 22997328254, 22997328233)); // fuer DEV
        //$objDirectInquiryDetail->setOptionalEquipmentIds(array(41836750977, 41836751016, 41836751118, 41836750881, 41836750884, 41836750974, 41836751007, 41836751013)); // fuer PROD
        $objDirectInquiryDetail->setBemerkung("Das ist ein Test ueber das DEV Deployment.");
        //$objDirectInquiryDetail->setBemerkung("Das ist ein Test über die PROD Loadbalancer api.ssis.de. Mit dem selben Aufpreiszubehör, wie die ursprünglichen Beispielmails aus dem Ticket.");

        $objDirectInquiryRequest = $objClient->getDirectInquiryRequest();
        $objDirectInquiryRequest->setCustomerData($objCustomerInquiryData);
        $objDirectInquiryRequest->setDetailData([$objDirectInquiryDetail]);

        echo "<h1>Direct Inquiry</h1>";
        echo '<pre>';
        $aryResponse = $objClient->direktinquiry($objDirectInquiryRequest);
        //$aryResponse[] = "Momentan funktioniert hier die API nicht richtig 2018-11-13";
        print_r($aryResponse);
        echo '</pre>';
    }

    /** Test direktsearch **/
    if (in_array('direktsearch', $aryTests)) {
        $objCustomerInquiryData = $objClient->getCustomerInquiryData();
        $objCustomerInquiryData->setPrivacyProtection(true);
        $objCustomerInquiryData->setSalutation('Herr');
        $objCustomerInquiryData->setName('Soisch');
        $objCustomerInquiryData->setFirstname('Martin');
        $objCustomerInquiryData->setCity('St. Ingbert');
        $objCustomerInquiryData->setPostcode('77777');
        $objCustomerInquiryData->setStreet('Warschauer Strasse');
        $objCustomerInquiryData->setEmail('msh@ssis.de');
        $objCustomerInquiryData->setTelefonDaytime('02166-8787877');

        $objAnfrage = $objClient->getAnfrage();
        $objAnfrage->setHersteller('BMW');
        $objAnfrage->setAngebotsnr();
        $objSearchInquiryRequest = $objClient->getSearchInquiryRequest();
        $objSearchInquiryRequest->setCustomerData($objCustomerInquiryData);
        $objSearchInquiryRequest->setSearchInquiryDetail($objAnfrage);
        echo "<h1>Direct Search</h1>";
        echo '<pre>';
        $aryResponse = $objClient->direktsearch($objSearchInquiryRequest);
        print_r($aryResponse);
        echo '</pre>';
    }

    /** Test freesearch **/
    if (in_array('freesearch', $aryTests)) {
        $objCustomerInquiryData = $objClient->getCustomerInquiryData();
        $objCustomerInquiryData->setPrivacyProtection(true);
        $objCustomerInquiryData->setSalutation('Herr');
        $objCustomerInquiryData->setName('Soisch');
        $objCustomerInquiryData->setFirstname('Martin');
        $objCustomerInquiryData->setCity('St. Ingbert');
        $objCustomerInquiryData->setPostcode('99999');
        $objCustomerInquiryData->setStreet('Warschauer Strasse');
        $objCustomerInquiryData->setEmail('msh@ssis.de');
        $objCustomerInquiryData->setTelefonDaytime('02166-8787877');

        $objAnfrage = $objClient->getAnfrage();
        $objAnfrage->setHersteller('BMW');

        $objSearchInquiryRequest = $objClient->getSearchInquiryRequest();
        $objSearchInquiryRequest->setCustomerData($objCustomerInquiryData);
        $objSearchInquiryRequest->setSearchInquiryDetail($objAnfrage);

        echo "<h1>Free Search</h1>";
        echo '<pre>';
        $aryResponse = $objClient->freesearch($objSearchInquiryRequest);
        print_r($aryResponse);
        echo '</pre>';
    }

    /** Test getInquiries **/
    if (in_array('getInquiries', $aryTests)) {
        $objGetInquiriesRequest = $objClient->getInquiriesRequest();
        $objGetInquiriesRequest->setFromDate('2018-11-01');
        //$objGetInquiriesRequest->setToDate('2018-12-01');
        echo "<h1>Get Inquiries</h1>";
        echo '<pre>';
        $aryResponse = $objClient->getInquiries($objGetInquiriesRequest, 9160);
        print_r($aryResponse);
        echo '</pre>';
    }

    if (in_array('check4duplicates', $aryTests)) {
        try {
            //Vorbereitung
            $apiClient = new Client();

            $vehicleSearch = $apiClient->getVehicleSearch();

            //Parameter
            $vehicleSearch->setCalculated(true);


            $paginationParameters = new PaginationParameters();
            $resultCount = 50;
            $paginationParameters->setResultCount($resultCount);
            $sort = 'VEHICLE_DATE_MODIFIED';
            #$sort = 'VEHICLE_MILEAGE';
            #$sort = 'VEHICLE_PRICE';
            #$paginationParameters->setSortType($sort);

            $page = 1;
            $maxPages = 9999999;

            $firstRound = true;
            $vehicleIds = array();

            do {
                $offset = $resultCount * ($page -1);
                $paginationParameters->setResultOffset($offset);

                $vehicleSearch->setPaginationParameters($paginationParameters);

                $vehicleSearchResult =  $apiClient->getVehicleSearchResult($vehicleSearch);
                $hits = $vehicleSearchResult->getHits();
                $singlePageHit = 0;
                foreach ($hits as $hit) {
                    $singlePageHit++;
                    $singleVehicleId = $hit->getVehicleId();
                    if(!key_exists($singleVehicleId, $vehicleIds)){
                        $vehicleIds[$singleVehicleId] = 1;
                    }else {
                        $vehicleIds[$singleVehicleId] = $vehicleIds[$singleVehicleId] + 1;
                        echo 'error on vehicleId => ' . $singleVehicleId . '<br>';
                        echo 'page => ' . $page . '<br>';
                        echo 'singlePageHit => ' . $singlePageHit . '<br>';

                    }
                }
                if($firstRound){
                    $hitsFound = $vehicleSearchResult->getHitsFound();
                    $maxPages =  $hitsFound / $resultCount / 10;
                    $firstRound = false;
                }

                $page++;

            } while ($page <= $maxPages);


            echo '<pre>';
            //var_dump($vehicleSearchResult);

            echo 'vehicleIds expected => ' . $hitsFound . '<br>';
            echo 'pages expected => ' . $hitsFound / $resultCount . '<br>' . '<br>';
            echo 'pages requested => ' . $page . '<br>';
            echo 'vehicleIds found => ' . count($vehicleIds) . '<br>';

            arsort($vehicleIds);
            print_r($vehicleIds);
            echo '</pre>';


        } catch (Swagger\Client\ApiException $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
            echo 'HTTP response headers: ', print_r($e->getResponseHeaders(), true), "\n";
            echo 'HTTP response body: ', print_r($e->getResponseBody(), true), "\n";
            echo 'HTTP status code: ', $e->getCode(), "\n";
        } catch (\Exception $ex) {
            echo $ex;
        }

    }

    /** Test Vehicle Details **/
    if (in_array('checkDiffBetweenSearchAndDetails', $aryTests)) {
        $objVehicleSearch = $objClient->getVehicleSearch();
        //$objVehicleSearch->setDealerIdCaller($dealerId);
        //$objVehicleSearch->setDoorCount([2]);

        $objClient->setPagParamSortType('date_modified');
        $objClient->setPagParamSortOrderAsc(TRUE);

        $objModelSearch = new \Swagger\Client\Model\ModelSearch();
        //$objModelSearch->setManufacturerTag('VOLVO');
        //$objVehicleSearch->setModelSearch([$objModelSearch]);

        $objClient->setPagParamResultCount(50);
        //$objClient->setPagParamResultOffset(0);
        /* @var $vehicleSearchResult \Swagger\Client\Model\VehicleSearchResult */
        $vehicleSearchResult = $objClient->getVehicleSearchResult($objVehicleSearch);

        echo '<h1>Vehicle Search Result</h1>';
        echo '<pre>';
        echo 'Found ' . sizeof($vehicleSearchResult->getHits()) . ' Hits';
        echo '</pre>';

        $vehicleIds = [];
        foreach ($vehicleSearchResult->getHits() as $vehicleHit) {
            $vehicleIds[] = $vehicleHit->getVehicleId();
            //echo '"' . $vehicleHit->getVehicleId() . '", ';
        }

        $objVehicleDetailsRequest = $objClient->getVehicleDetailsRequest();
        $objVehicleDetailsRequest->setVehicleIds($vehicleIds);
        //echo '<pre>'; var_dump($objVehicleDetailsRequest); exit;
        $vehicleResponse = $objClient->getVehicleDetails($objVehicleDetailsRequest);
        //echo '<pre>'; var_dump($vehicleResponse); exit;
        echo '<h1>Vehicle Details</h1>';
        echo '<pre>';
        //print_r($objClient->getVehicleDetails($objVehicleDetailsRequest));
        echo 'Found ' . sizeof($vehicleResponse->getVehicles()) . ' Hits';
        echo '</pre>';
    }
    
    /** Test Metadata **/
    if (in_array('Metadata', $aryTests)) {
        echo '<h1>Metadata</h1>';
        echo '<pre>';
        print_r($objClient->getLastMetadata());
        echo '</pre>';
    }
    
} catch (Swagger\Client\ApiException $e) {
    echo 'Caught exception: ', $e->getMessage(), "\n";
    echo 'HTTP response headers: ', print_r($e->getResponseHeaders(), true), "\n";
    echo 'HTTP response body: ', print_r($e->getResponseBody(), true), "\n";
    echo 'HTTP status code: ', $e->getCode(), "\n";
} catch (\Exception $ex) {
    echo $ex;
}