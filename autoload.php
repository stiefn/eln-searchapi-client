<?php
if (!defined('PATH_ROOT')) {
    define('PATH_ROOT', __DIR__);
}

require_once(PATH_ROOT . '/vendor/autoload.php');

spl_autoload_register(function ($classname) {
    $classname = str_replace('\\', DIRECTORY_SEPARATOR, $classname);
    if (strpos($classname, 'service' . DIRECTORY_SEPARATOR . 'ssis' . DIRECTORY_SEPARATOR . 'api' . DIRECTORY_SEPARATOR) !== FALSE) {
        $filename =  PATH_ROOT . DIRECTORY_SEPARATOR . $classname . ".php";
        include_once($filename);
    }
});