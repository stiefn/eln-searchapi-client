<?php
/**
 * AccessTokenValidation
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * ELN Search-API
 *
 * Description
 *
 * The version of the OpenAPI document: 0.0.x
 * Contact: technik@ssis.de
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.1.3
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;
use \Swagger\Client\ObjectSerializer;

/**
 * AccessTokenValidation Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class AccessTokenValidation implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'AccessTokenValidation';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'initial_validation_successful' => 'bool',
        'client_id' => 'string',
        'client_ip_address' => 'string',
        'client_subject' => '\Swagger\Client\Model\UserSubject',
        'token_key' => 'string',
        'token_type' => 'string',
        'token_grant_type' => 'string',
        'token_issued_at' => 'int',
        'token_lifetime' => 'int',
        'token_issuer' => 'string',
        'token_subject' => '\Swagger\Client\Model\UserSubject',
        'token_scopes' => '\Swagger\Client\Model\OAuthPermission[]',
        'audiences' => 'string[]',
        'client_code_verifier' => 'string',
        'extra_props' => 'map[string,string]',
        'client_confidential' => 'bool'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPIFormats = [
        'initial_validation_successful' => null,
        'client_id' => null,
        'client_ip_address' => null,
        'client_subject' => null,
        'token_key' => null,
        'token_type' => null,
        'token_grant_type' => null,
        'token_issued_at' => 'int64',
        'token_lifetime' => 'int64',
        'token_issuer' => null,
        'token_subject' => null,
        'token_scopes' => null,
        'audiences' => null,
        'client_code_verifier' => null,
        'extra_props' => null,
        'client_confidential' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'initial_validation_successful' => 'initialValidationSuccessful',
        'client_id' => 'clientId',
        'client_ip_address' => 'clientIpAddress',
        'client_subject' => 'clientSubject',
        'token_key' => 'tokenKey',
        'token_type' => 'tokenType',
        'token_grant_type' => 'tokenGrantType',
        'token_issued_at' => 'tokenIssuedAt',
        'token_lifetime' => 'tokenLifetime',
        'token_issuer' => 'tokenIssuer',
        'token_subject' => 'tokenSubject',
        'token_scopes' => 'tokenScopes',
        'audiences' => 'audiences',
        'client_code_verifier' => 'clientCodeVerifier',
        'extra_props' => 'extraProps',
        'client_confidential' => 'clientConfidential'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'initial_validation_successful' => 'setInitialValidationSuccessful',
        'client_id' => 'setClientId',
        'client_ip_address' => 'setClientIpAddress',
        'client_subject' => 'setClientSubject',
        'token_key' => 'setTokenKey',
        'token_type' => 'setTokenType',
        'token_grant_type' => 'setTokenGrantType',
        'token_issued_at' => 'setTokenIssuedAt',
        'token_lifetime' => 'setTokenLifetime',
        'token_issuer' => 'setTokenIssuer',
        'token_subject' => 'setTokenSubject',
        'token_scopes' => 'setTokenScopes',
        'audiences' => 'setAudiences',
        'client_code_verifier' => 'setClientCodeVerifier',
        'extra_props' => 'setExtraProps',
        'client_confidential' => 'setClientConfidential'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'initial_validation_successful' => 'getInitialValidationSuccessful',
        'client_id' => 'getClientId',
        'client_ip_address' => 'getClientIpAddress',
        'client_subject' => 'getClientSubject',
        'token_key' => 'getTokenKey',
        'token_type' => 'getTokenType',
        'token_grant_type' => 'getTokenGrantType',
        'token_issued_at' => 'getTokenIssuedAt',
        'token_lifetime' => 'getTokenLifetime',
        'token_issuer' => 'getTokenIssuer',
        'token_subject' => 'getTokenSubject',
        'token_scopes' => 'getTokenScopes',
        'audiences' => 'getAudiences',
        'client_code_verifier' => 'getClientCodeVerifier',
        'extra_props' => 'getExtraProps',
        'client_confidential' => 'getClientConfidential'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['initial_validation_successful'] = isset($data['initial_validation_successful']) ? $data['initial_validation_successful'] : null;
        $this->container['client_id'] = isset($data['client_id']) ? $data['client_id'] : null;
        $this->container['client_ip_address'] = isset($data['client_ip_address']) ? $data['client_ip_address'] : null;
        $this->container['client_subject'] = isset($data['client_subject']) ? $data['client_subject'] : null;
        $this->container['token_key'] = isset($data['token_key']) ? $data['token_key'] : null;
        $this->container['token_type'] = isset($data['token_type']) ? $data['token_type'] : null;
        $this->container['token_grant_type'] = isset($data['token_grant_type']) ? $data['token_grant_type'] : null;
        $this->container['token_issued_at'] = isset($data['token_issued_at']) ? $data['token_issued_at'] : null;
        $this->container['token_lifetime'] = isset($data['token_lifetime']) ? $data['token_lifetime'] : null;
        $this->container['token_issuer'] = isset($data['token_issuer']) ? $data['token_issuer'] : null;
        $this->container['token_subject'] = isset($data['token_subject']) ? $data['token_subject'] : null;
        $this->container['token_scopes'] = isset($data['token_scopes']) ? $data['token_scopes'] : null;
        $this->container['audiences'] = isset($data['audiences']) ? $data['audiences'] : null;
        $this->container['client_code_verifier'] = isset($data['client_code_verifier']) ? $data['client_code_verifier'] : null;
        $this->container['extra_props'] = isset($data['extra_props']) ? $data['extra_props'] : null;
        $this->container['client_confidential'] = isset($data['client_confidential']) ? $data['client_confidential'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets initial_validation_successful
     *
     * @return bool|null
     */
    public function getInitialValidationSuccessful()
    {
        return $this->container['initial_validation_successful'];
    }

    /**
     * Sets initial_validation_successful
     *
     * @param bool|null $initial_validation_successful initial_validation_successful
     *
     * @return $this
     */
    public function setInitialValidationSuccessful($initial_validation_successful)
    {
        $this->container['initial_validation_successful'] = $initial_validation_successful;

        return $this;
    }

    /**
     * Gets client_id
     *
     * @return string|null
     */
    public function getClientId()
    {
        return $this->container['client_id'];
    }

    /**
     * Sets client_id
     *
     * @param string|null $client_id client_id
     *
     * @return $this
     */
    public function setClientId($client_id)
    {
        $this->container['client_id'] = $client_id;

        return $this;
    }

    /**
     * Gets client_ip_address
     *
     * @return string|null
     */
    public function getClientIpAddress()
    {
        return $this->container['client_ip_address'];
    }

    /**
     * Sets client_ip_address
     *
     * @param string|null $client_ip_address client_ip_address
     *
     * @return $this
     */
    public function setClientIpAddress($client_ip_address)
    {
        $this->container['client_ip_address'] = $client_ip_address;

        return $this;
    }

    /**
     * Gets client_subject
     *
     * @return \Swagger\Client\Model\UserSubject|null
     */
    public function getClientSubject()
    {
        return $this->container['client_subject'];
    }

    /**
     * Sets client_subject
     *
     * @param \Swagger\Client\Model\UserSubject|null $client_subject client_subject
     *
     * @return $this
     */
    public function setClientSubject($client_subject)
    {
        $this->container['client_subject'] = $client_subject;

        return $this;
    }

    /**
     * Gets token_key
     *
     * @return string|null
     */
    public function getTokenKey()
    {
        return $this->container['token_key'];
    }

    /**
     * Sets token_key
     *
     * @param string|null $token_key token_key
     *
     * @return $this
     */
    public function setTokenKey($token_key)
    {
        $this->container['token_key'] = $token_key;

        return $this;
    }

    /**
     * Gets token_type
     *
     * @return string|null
     */
    public function getTokenType()
    {
        return $this->container['token_type'];
    }

    /**
     * Sets token_type
     *
     * @param string|null $token_type token_type
     *
     * @return $this
     */
    public function setTokenType($token_type)
    {
        $this->container['token_type'] = $token_type;

        return $this;
    }

    /**
     * Gets token_grant_type
     *
     * @return string|null
     */
    public function getTokenGrantType()
    {
        return $this->container['token_grant_type'];
    }

    /**
     * Sets token_grant_type
     *
     * @param string|null $token_grant_type token_grant_type
     *
     * @return $this
     */
    public function setTokenGrantType($token_grant_type)
    {
        $this->container['token_grant_type'] = $token_grant_type;

        return $this;
    }

    /**
     * Gets token_issued_at
     *
     * @return int|null
     */
    public function getTokenIssuedAt()
    {
        return $this->container['token_issued_at'];
    }

    /**
     * Sets token_issued_at
     *
     * @param int|null $token_issued_at token_issued_at
     *
     * @return $this
     */
    public function setTokenIssuedAt($token_issued_at)
    {
        $this->container['token_issued_at'] = $token_issued_at;

        return $this;
    }

    /**
     * Gets token_lifetime
     *
     * @return int|null
     */
    public function getTokenLifetime()
    {
        return $this->container['token_lifetime'];
    }

    /**
     * Sets token_lifetime
     *
     * @param int|null $token_lifetime token_lifetime
     *
     * @return $this
     */
    public function setTokenLifetime($token_lifetime)
    {
        $this->container['token_lifetime'] = $token_lifetime;

        return $this;
    }

    /**
     * Gets token_issuer
     *
     * @return string|null
     */
    public function getTokenIssuer()
    {
        return $this->container['token_issuer'];
    }

    /**
     * Sets token_issuer
     *
     * @param string|null $token_issuer token_issuer
     *
     * @return $this
     */
    public function setTokenIssuer($token_issuer)
    {
        $this->container['token_issuer'] = $token_issuer;

        return $this;
    }

    /**
     * Gets token_subject
     *
     * @return \Swagger\Client\Model\UserSubject|null
     */
    public function getTokenSubject()
    {
        return $this->container['token_subject'];
    }

    /**
     * Sets token_subject
     *
     * @param \Swagger\Client\Model\UserSubject|null $token_subject token_subject
     *
     * @return $this
     */
    public function setTokenSubject($token_subject)
    {
        $this->container['token_subject'] = $token_subject;

        return $this;
    }

    /**
     * Gets token_scopes
     *
     * @return \Swagger\Client\Model\OAuthPermission[]|null
     */
    public function getTokenScopes()
    {
        return $this->container['token_scopes'];
    }

    /**
     * Sets token_scopes
     *
     * @param \Swagger\Client\Model\OAuthPermission[]|null $token_scopes token_scopes
     *
     * @return $this
     */
    public function setTokenScopes($token_scopes)
    {
        $this->container['token_scopes'] = $token_scopes;

        return $this;
    }

    /**
     * Gets audiences
     *
     * @return string[]|null
     */
    public function getAudiences()
    {
        return $this->container['audiences'];
    }

    /**
     * Sets audiences
     *
     * @param string[]|null $audiences audiences
     *
     * @return $this
     */
    public function setAudiences($audiences)
    {
        $this->container['audiences'] = $audiences;

        return $this;
    }

    /**
     * Gets client_code_verifier
     *
     * @return string|null
     */
    public function getClientCodeVerifier()
    {
        return $this->container['client_code_verifier'];
    }

    /**
     * Sets client_code_verifier
     *
     * @param string|null $client_code_verifier client_code_verifier
     *
     * @return $this
     */
    public function setClientCodeVerifier($client_code_verifier)
    {
        $this->container['client_code_verifier'] = $client_code_verifier;

        return $this;
    }

    /**
     * Gets extra_props
     *
     * @return map[string,string]|null
     */
    public function getExtraProps()
    {
        return $this->container['extra_props'];
    }

    /**
     * Sets extra_props
     *
     * @param map[string,string]|null $extra_props extra_props
     *
     * @return $this
     */
    public function setExtraProps($extra_props)
    {
        $this->container['extra_props'] = $extra_props;

        return $this;
    }

    /**
     * Gets client_confidential
     *
     * @return bool|null
     */
    public function getClientConfidential()
    {
        return $this->container['client_confidential'];
    }

    /**
     * Sets client_confidential
     *
     * @param bool|null $client_confidential client_confidential
     *
     * @return $this
     */
    public function setClientConfidential($client_confidential)
    {
        $this->container['client_confidential'] = $client_confidential;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


