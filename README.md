# SSiS API Client PHP

Base created with openAPI (Swagger 3.0) generator. See [openAPI-Readme](README_OAPI.md).

To simplify usage, there is a wrapper class "[service.ssis.api.Client.php](docs/ssis/Client.md)".

The resource classes [DealersApi](docs/Api/DealersApi.md) and [VehiclesApi](docs/Api/VehiclesApi.md) are wrapped by [DealersResource](docs/ssis/DealersResource.md) and [VehiclesResource](docs/ssis/VehiclesResource.md) too.

To integrate the API Client with your project, you have to import the [autoload.php](./autoload.php).


## How to use this client

Copy all API Client PHP files and directories to your project directory.

Make sure to have "composer" installed on your server. [Get composer](https://getcomposer.org/download/)

Open your terminal.

Change into API Client PHP directory and run "composer install". 

The vendor directory will be created and additional libraries will be downloaded.

Rename the "[config.php.dist](config.php.dist)" to "config.php".

Edit the "config.php" and enter your username, password and dealer ID to the $cfgApi array


For using API Client in your project, you have to include [autoload.php](autoload.php) and use the namespace "service\ssis\api\Client".
In [public/clienttest.php](public/clienttest.php) you can find some examples how to use the [Client](docs/ssis/Client.md).


## Documentation for Wrapper Classes

[Client](docs/ssis/Client.md)

[DealersResource](docs/ssis/DealersResource.md)

[VehiclesResource](docs/ssis/VehiclesResource.md)


You can find our [ELN Search-API Documentation](https://api.ssis.de:8443/api/) [here](https://api.ssis.de:8443/api/).
 