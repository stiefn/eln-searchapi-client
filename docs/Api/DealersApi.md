# Swagger\Client\DealersApi

All URIs are relative to *https://api.ssis.de:8443/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getDealerDetails**](DealersApi.md#getDealerDetails) | **POST** /dealers/details | Get Dealerdetails
[**searchDealer**](DealersApi.md#searchDealer) | **POST** /dealers/search | search Dealers



## getDealerDetails

> \Swagger\Client\Model\DealerResponseWithMeta getDealerDetails($dealer_details_request)

Get Dealerdetails

Get list of dealerdetails

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basicAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Swagger\Client\Api\DealersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$dealer_details_request = new \Swagger\Client\Model\DealerDetailsRequest(); // \Swagger\Client\Model\DealerDetailsRequest | dealerIds

try {
    $result = $apiInstance->getDealerDetails($dealer_details_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DealersApi->getDealerDetails: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealer_details_request** | [**\Swagger\Client\Model\DealerDetailsRequest**](../Model/DealerDetailsRequest.md)| dealerIds |

### Return type

[**\Swagger\Client\Model\DealerResponseWithMeta**](../Model/DealerResponseWithMeta.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json;charset=utf-8
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchDealer

> \Swagger\Client\Model\DealerSearchResultWithMeta searchDealer($dealer_search)

search Dealers

search list of dealers

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basicAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Swagger\Client\Api\DealersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$dealer_search = new \Swagger\Client\Model\DealerSearch(); // \Swagger\Client\Model\DealerSearch | dealerIds

try {
    $result = $apiInstance->searchDealer($dealer_search);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DealersApi->searchDealer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealer_search** | [**\Swagger\Client\Model\DealerSearch**](../Model/DealerSearch.md)| dealerIds |

### Return type

[**\Swagger\Client\Model\DealerSearchResultWithMeta**](../Model/DealerSearchResultWithMeta.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json;charset=utf-8
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

