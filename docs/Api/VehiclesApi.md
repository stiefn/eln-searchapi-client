# Swagger\Client\VehiclesApi

All URIs are relative to *https://api.ssis.de:8443/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getVehicleAggregationTags**](VehiclesApi.md#getVehicleAggregationTags) | **POST** /dealers/{dealerid}/vehicles/aggregationtags | 
[**getVehicleDetails**](VehiclesApi.md#getVehicleDetails) | **POST** /dealers/{dealerid}/vehicles/details | Get Vehicledetails
[**searchDealerVehicles**](VehiclesApi.md#searchDealerVehicles) | **POST** /dealers/{dealerid}/vehicles/search | search Vehicles



## getVehicleAggregationTags

> \Swagger\Client\Model\TagSearchResultWithMeta getVehicleAggregationTags($dealerid, $tag_search)



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basicAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Swagger\Client\Api\VehiclesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$dealerid = 56; // int | 
$tag_search = new \Swagger\Client\Model\TagSearch(); // \Swagger\Client\Model\TagSearch | tags

try {
    $result = $apiInstance->getVehicleAggregationTags($dealerid, $tag_search);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VehiclesApi->getVehicleAggregationTags: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerid** | **int**|  |
 **tag_search** | [**\Swagger\Client\Model\TagSearch**](../Model/TagSearch.md)| tags |

### Return type

[**\Swagger\Client\Model\TagSearchResultWithMeta**](../Model/TagSearchResultWithMeta.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json;charset=utf-8
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getVehicleDetails

> \Swagger\Client\Model\VehicleResponseWithMeta getVehicleDetails($dealerid, $vehicle_details_request)

Get Vehicledetails

Get list of vehicledetails

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basicAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Swagger\Client\Api\VehiclesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$dealerid = 56; // int | 
$vehicle_details_request = new \Swagger\Client\Model\VehicleDetailsRequest(); // \Swagger\Client\Model\VehicleDetailsRequest | vehicleIds

try {
    $result = $apiInstance->getVehicleDetails($dealerid, $vehicle_details_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VehiclesApi->getVehicleDetails: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerid** | **int**|  |
 **vehicle_details_request** | [**\Swagger\Client\Model\VehicleDetailsRequest**](../Model/VehicleDetailsRequest.md)| vehicleIds |

### Return type

[**\Swagger\Client\Model\VehicleResponseWithMeta**](../Model/VehicleResponseWithMeta.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json;charset=utf-8
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchDealerVehicles

> \Swagger\Client\Model\VehicleSearchResultWithMeta searchDealerVehicles($dealerid, $vehicle_search)

search Vehicles

search list of vehicles

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basicAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Swagger\Client\Api\VehiclesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$dealerid = 56; // int | 
$vehicle_search = new \Swagger\Client\Model\VehicleSearch(); // \Swagger\Client\Model\VehicleSearch | search list of vehicles

try {
    $result = $apiInstance->searchDealerVehicles($dealerid, $vehicle_search);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VehiclesApi->searchDealerVehicles: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerid** | **int**|  |
 **vehicle_search** | [**\Swagger\Client\Model\VehicleSearch**](../Model/VehicleSearch.md)| search list of vehicles |

### Return type

[**\Swagger\Client\Model\VehicleSearchResultWithMeta**](../Model/VehicleSearchResultWithMeta.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json;charset=utf-8
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

