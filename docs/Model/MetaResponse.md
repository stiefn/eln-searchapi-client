# # MetaResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**build** | [**\Swagger\Client\Model\BuildResponse**](BuildResponse.md) |  | [optional] 
**request** | [**\Swagger\Client\Model\RequestResponse**](RequestResponse.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


