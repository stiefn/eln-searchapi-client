# # VehicleSearchResultWithMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**\Swagger\Client\Model\MetaResponse**](MetaResponse.md) |  | [optional] 
**response** | [**\Swagger\Client\Model\VehicleSearchResult**](VehicleSearchResult.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


