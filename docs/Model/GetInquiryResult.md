# # GetInquiryResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**inquiry_objects** | [**\Swagger\Client\Model\InquiryObject[]**](InquiryObject.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


