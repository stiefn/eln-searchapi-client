# # ChangeInquiryRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**inquiry_id** | **int** |  | [optional] 
**processed** | **bool** |  | [optional] 
**seen** | **bool** |  | [optional] 
**sent** | **bool** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


