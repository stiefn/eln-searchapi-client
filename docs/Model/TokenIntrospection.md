# # TokenIntrospection

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | **bool** |  | [optional] 
**scope** | **string** |  | [optional] 
**client_id** | **string** |  | [optional] 
**username** | **string** |  | [optional] 
**token_type** | **string** |  | [optional] 
**iat** | **int** |  | [optional] 
**exp** | **int** |  | [optional] 
**nbf** | **int** |  | [optional] 
**sub** | **string** |  | [optional] 
**aud** | **string[]** |  | [optional] 
**iss** | **string** |  | [optional] 
**jti** | **string** |  | [optional] 
**extensions** | **map[string,string]** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


