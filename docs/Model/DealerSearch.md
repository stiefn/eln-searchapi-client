# # DealerSearch

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealer_ids** | **int[]** |  | [optional] 
**company** | **string** |  | [optional] 
**country_iso** | **string** |  | [optional] 
**include_non_public_dealer_range** | **bool** |  | [optional] 
**products** | [**\Swagger\Client\Model\ProductSearch[]**](ProductSearch.md) |  | [optional] 
**geo_distance_search** | [**\Swagger\Client\Model\GeoDistanceSearch**](GeoDistanceSearch.md) |  | [optional] 
**pagination_parameters** | [**\Swagger\Client\Model\PaginationParameters**](PaginationParameters.md) |  | [optional] 
**date_modified** | **string[]** |  | [optional] 
**city** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


