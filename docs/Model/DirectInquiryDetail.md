# # DirectInquiryDetail

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vehicle_id** | **string** |  | [optional] 
**optional_equipment_ids** | **int[]** | A list of the selected optional equipment id&#39;s from the inquiry vehicle. | [optional] 
**bemerkung** | **string** |  | [optional] 
**sonstiges** | **string** |  | [optional] 
**cargarantie** | **int** |  | [optional] 
**kaufzeitraum** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


