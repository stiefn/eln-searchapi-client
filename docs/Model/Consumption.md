# # Consumption

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**consumption_city** | **float** |  | [optional] 
**consumption_outskirts** | **float** |  | [optional] 
**consumption_total** | **float** |  | [optional] 
**consumption_electricity** | **float** |  | [optional] 
**co2_emission** | **float** |  | [optional] 
**co2_efficiency** | **string** |  | [optional] 
**energy_carrier_cost** | **float** |  | [optional] 
**fuel_types** | [**\Swagger\Client\Model\FuelType[]**](FuelType.md) |  | [optional] 
**wltp_emission** | **int** | WLTP Emission. Ganzzahl in g/km (wird auf 0 gesetzt bei Elektro). | [optional] 
**wltp_combined** | **float** | WLTP Verbrauch kombiniert. in l/100km (bei Erdgas in kg/100km, bei Elektro in kWh/100km) | [optional] 
**wltp_slow** | **float** | WLTP Verbrauch langsam. in l/100km | [optional] 
**wltp_middle** | **float** | WLTP Verbrauch mittel. in l/100km | [optional] 
**wltp_fast** | **float** | WLTP Verbrauch schnell. in l/100km | [optional] 
**wltp_very_fast** | **float** | WLTP Verbrauch sehr schnell. in l/100km | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


