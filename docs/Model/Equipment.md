# # Equipment

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**text** | **string** |  | [optional] 
**category_tag** | **string** |  | [optional] 
**category_text** | **string** |  | [optional] 
**prices** | [**map[string,\Swagger\Client\Model\Price]**](Price.md) |  | [optional] 
**optional** | **bool** |  | [optional] 
**not_for_resellers** | **bool** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


