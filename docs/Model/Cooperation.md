# # Cooperation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cooperation** | **string** |  | [optional] 
**status** | **string** |  | [optional] 
**user_id** | **string** |  | [optional] 
**email** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


