# # SyncVehiclesDBElasticResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**num_db_vehicles** | **int** |  | [optional] 
**num_es_vehicles** | **int** |  | [optional] 
**num_db_vehicles_not_in_es** | **int** |  | [optional] 
**num_es_vehicles_not_in_db** | **int** |  | [optional] 
**vehicle_infos** | [**\Swagger\Client\Model\VehicleInfo[]**](VehicleInfo.md) |  | [optional] 
**messages** | **string[]** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


