# # Outlet

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**outlet_attachment** | **string** |  | [optional] 
**outlet_mail_body** | **string** |  | [optional] 
**outlet_mail_id** | **string** |  | [optional] 
**outlet_sender_email** | **string** |  | [optional] 
**outlet_sender_name** | **string** |  | [optional] 
**vehicle_type** | **string** |  | [optional] 
**date_indexed** | **string** |  | [optional] 
**outlet_date_transmitted** | **string** |  | [optional] 
**outlet_subject** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


