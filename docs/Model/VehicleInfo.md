# # VehicleInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vehicle_id** | **int** |  | [optional] 
**susuuid** | **string** |  | [optional] 
**date_last_indexed** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


