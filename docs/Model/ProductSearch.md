# # ProductSearch

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tag** | **string** |  | [optional] 
**is_blocked** | **bool** |  | [optional] 
**is_testphase** | **bool** |  | [optional] 
**product_options** | [**\Swagger\Client\Model\ProductOptionSearch[]**](ProductOptionSearch.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


