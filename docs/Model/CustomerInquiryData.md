# # CustomerInquiryData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**language** | **int** |  | [optional] 
**customer_id** | **string** |  | [optional] 
**salutation** | **string** |  | [optional] 
**name** | **string** | max. length 50 chars | 
**firstname** | **string** | max. length 50 chars | [optional] 
**street** | **string** |  | [optional] 
**postcode** | **string** |  | [optional] 
**city** | **string** |  | [optional] 
**country** | **string** |  | [optional] 
**telefon_daytime** | **string** |  | [optional] 
**telefon_evening** | **string** |  | [optional] 
**email** | **string** | max. length 50 chars | 
**fax** | **string** |  | [optional] 
**age** | **string** |  | [optional] 
**profession** | **string** |  | [optional] 
**purchase_type** | **string** |  | [optional] 
**company** | **string** |  | [optional] 
**customer_id_old** | **string** |  | [optional] 
**copied** | **bool** |  | [optional] 
**privacy_protection** | **bool** | The Customer accept our privacy protection rules, true or false mandatory, only agreed Request will be stored and processed. | 
**date_created** | **string** |  | [optional] 
**date_modified** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


