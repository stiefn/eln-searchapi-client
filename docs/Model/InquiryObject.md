# # InquiryObject

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer** | [**\Swagger\Client\Model\CustomerInquiryData**](CustomerInquiryData.md) |  | [optional] 
**anfrage** | [**\Swagger\Client\Model\Anfrage**](Anfrage.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


