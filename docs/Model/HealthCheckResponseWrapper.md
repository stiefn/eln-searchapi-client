# # HealthCheckResponseWrapper

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**\Swagger\Client\Model\MetaResponse**](MetaResponse.md) |  | [optional] 
**elasticsearch** | [**\Swagger\Client\Model\HealthCheckResult**](HealthCheckResult.md) |  | [optional] 
**persistence** | [**\Swagger\Client\Model\HealthCheckResult**](HealthCheckResult.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


