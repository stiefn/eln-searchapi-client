# # Geolocation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**latitude** | **double** |  | [optional] 
**longitude** | **double** |  | [optional] 
**valid** | **bool** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


