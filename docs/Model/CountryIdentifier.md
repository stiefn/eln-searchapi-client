# # CountryIdentifier

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**license_plate_code** | **string** |  | [optional] 
**iso_code** | **string** |  | [optional] 
**translation** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


