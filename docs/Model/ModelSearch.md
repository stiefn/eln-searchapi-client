# # ModelSearch

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**manufacturer_tag** | **string** |  | [optional] 
**series_tag** | **string** |  | [optional] 
**model_query** | **string[]** |  | [optional] 
**exclude** | **bool** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


