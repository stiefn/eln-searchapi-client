# # AccessTokenValidation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**initial_validation_successful** | **bool** |  | [optional] 
**client_id** | **string** |  | [optional] 
**client_ip_address** | **string** |  | [optional] 
**client_subject** | [**\Swagger\Client\Model\UserSubject**](UserSubject.md) |  | [optional] 
**token_key** | **string** |  | [optional] 
**token_type** | **string** |  | [optional] 
**token_grant_type** | **string** |  | [optional] 
**token_issued_at** | **int** |  | [optional] 
**token_lifetime** | **int** |  | [optional] 
**token_issuer** | **string** |  | [optional] 
**token_subject** | [**\Swagger\Client\Model\UserSubject**](UserSubject.md) |  | [optional] 
**token_scopes** | [**\Swagger\Client\Model\OAuthPermission[]**](OAuthPermission.md) |  | [optional] 
**audiences** | **string[]** |  | [optional] 
**client_code_verifier** | **string** |  | [optional] 
**extra_props** | **map[string,string]** |  | [optional] 
**client_confidential** | **bool** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


