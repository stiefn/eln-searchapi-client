# # DealerResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealers** | [**\Swagger\Client\Model\Dealer[]**](Dealer.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


