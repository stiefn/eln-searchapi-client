# # BusinesscardImage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_nr** | **int** |  | [optional] 
**image_url** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


