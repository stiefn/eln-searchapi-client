# # PaginationParameters

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result_count** | **int** |  | [optional] 
**result_offset** | **int** |  | [optional] 
**sort_type** | **string** |  | [optional] 
**sort_order_asc** | **bool** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


