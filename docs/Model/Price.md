# # Price

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** |  | [optional] 
**value** | **float** |  | [optional] 
**vat_rate** | **float** |  | [optional] 
**vat_included** | **bool** |  | [optional] 
**currency_iso** | **string** |  | [optional] 
**currency_symbol** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


