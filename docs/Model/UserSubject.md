# # UserSubject

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**login** | **string** |  | [optional] 
**id** | **string** |  | [optional] 
**roles** | **string[]** |  | [optional] 
**properties** | **map[string,string]** |  | [optional] 
**authentication_method** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


