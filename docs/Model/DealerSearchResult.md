# # DealerSearchResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hits_found** | [**object**](.md) |  | [optional] 
**hits** | [**\Swagger\Client\Model\DealerSearchHit[]**](DealerSearchHit.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


