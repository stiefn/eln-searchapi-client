# ApiConfig
***
This class cast the configuration from [config.php](./config.md) to an configuration object, to use in the [Client](./Client.md) class.


Method | Return Type | Description
------------- | ------------- | -------------
[**setConfig**](ApiConfig.md#setconfig) | **[ApiConfig](ApiConfig.md)** | Set the configuration array
[**getConfig**](ApiConfig.md#getconfig) | array | Return the configuration array
[**setHost**](ApiConfig.md#sethost) | **[ApiConfig](ApiConfig.md)** | Set the host in the config array
[**setTempFolderPath**](ApiConfig.md#settempfolderpath) | **[ApiConfig](ApiConfig.md)** | Set the temp folder path in the config array
[**setDebugFile**](ApiConfig.md#setdebugfile) | **[ApiConfig](ApiConfig.md)** | Set the debug file path in the config array
[**setUsername**](ApiConfig.md#setusername) | **[ApiConfig](ApiConfig.md)** | Set the username in the config array
[**setPassword**](ApiConfig.md#setpassword) | **[ApiConfig](ApiConfig.md)** | Set the password in the config array
[**setDealerId**](ApiConfig.md#setdealerid) | **[ApiConfig](ApiConfig.md)** | Set the dealer id in the config array



# **setConfig**
> ApiConfig setConfig(array $aryConfig)

### Parameters
Name | Type | Description  
------------- | ------------- | ------------- 
 aryConfig | array | Set the configuration array 

### Return type
[ApiConfig](ApiConfig.md)


<a name="getconfig"></a>
# **getConfig**
> array getConfig()

### Return type
array


# **setHost**
> ApiConfig setHost(string $host)

### Parameters
Name | Type | Description 
------------- | ------------- | ------------- 
 host | string | Set the host in the config array 

### Return type
[ApiConfig](ApiConfig.md)


# **setTempFolderPath**
> ApiConfig setTempFolderPath(string $path)

### Parameters
Name | Type | Description 
------------- | ------------- | ------------- 
 path | string | Set the temp folder path in the config array 

### Return type
[ApiConfig](ApiConfig.md)



# **setDebugFile**
> ApiConfig setDebugFile(string $file)

### Parameters
Name | Type | Description 
------------- | ------------- | ------------- 
 file | string | Set the debug file path in the config array

### Return type
[ApiConfig](ApiConfig.md)


# **setUsername**
> ApiConfig setUsername(string $username)

### Parameters
Name | Type | Description 
------------- | ------------- | ------------- 
 username | string | Set the username in the config array

### Return type
[ApiConfig](ApiConfig.md)


# **setPassword**
> ApiConfig setPassword(string $password)

### Parameters
Name | Type | Description 
------------- | ------------- | ------------- 
 password | string | Set the password in the config array

### Return type
[ApiConfig](ApiConfig.md)


# **setDealerId**
> ApiConfig setDealerId(string $dealerId)

### Parameters
Name | Type | Description 
------------- | ------------- | ------------- 
 dealerId | string | Set the dealer id in the config array 

### Return type
[ApiConfig](ApiConfig.md)





```php
[
    'host' => 'https://api.ssis.de:8443/api',
    'temp_folder_path' => '/var/tmp/php/',
    'debug_file' => '/var/tmp/php_debug.log',
    'username' => '',
    'password' => '',
    'dealerid' => 0
]
```