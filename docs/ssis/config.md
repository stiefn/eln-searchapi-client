# config.php
***
First you must rename the [config.php.dist](../../config.php.dist) to **config.php**

Then you must enter here your credentials.

```php
$cfgApi = [
    'host' => 'https://api.ssis.de:8443/api',
    'temp_folder_path' => '/var/tmp/php/',
    'debug_file' => '/var/tmp/php_debug.log',
    'username' => '',
    'password' => '',
    'dealerid' => 0000
];
```