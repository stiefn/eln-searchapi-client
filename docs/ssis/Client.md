# Client
***
This class simplifies the usage of the swagger-generated api classes.

It includes ia. the HTTP request methods from the [**DealersApi**](../Api/DealersApi.md) and the [**VehiclesApi**](../Api/VehiclesApi.md)

All URIs are relative to *https://api.ssis.de:8443/api*

In the class contructor the configuration is been set. 

### Constructor

> public function __construct(string $username = null, string $password = null, int $dealerId = null)

The parameters are optional, if you are using the [config.php](config.md).

If you are do not use the config.php, you must set the host explicitly with [**setHost**](#markdown-header-sethost) method.



#### [**Class Constants**](#markdown-header-constants-for-vehicle-aggregation-types)



Method | Return Type | Description
------------- | ------------- | -------------
[**getClientVersion**](#markdown-header-getclientversion) | **String** | Returns the version of the API Client
[**getHost**](#markdown-header-gethost) | **String** | Returns the API Url
[**setHost**](#markdown-header-sethost) | **[Client](Client.md)** | Sets the API Url
[**getDealerId**](#markdown-header-getdealerid) | **Integer**  | Returns the Dealer ID
[**setDealerId**](#markdown-header-setdealerid) | **[Client](Client.md)** | Sets the Dealer ID
[**getPagParams**](#markdown-header-getpagparams) | **Array**  | Returns the Pagination Parameters
[**setPaginationParameters**](#markdown-header-setpaginationparameters) | **[Client](Client.md)** | Set the Pagination Parameters Object
[**getPaginationParameters**](#markdown-header-getpaginationparameters) | **[PaginationParameters](../Model/PaginationParameters.md)** | Returns the Pagination Parameters Object
[**setPagParamResultCount**](#markdown-header-setpagparamresultcount) | **[Client](Client.md)** | Set the number of return values
[**setPagParamResultOffset**](#markdown-header-setpagparamresultoffset) | **[Client](Client.md)** | Set the offset of the return values
[**setPagParamSortType**](#markdown-header-setpagparamsorttype) | **[Client](Client.md)** | Set the fieldname for sort (default: id)
[**setPagParamSortOrderAsc**](#markdown-header-setpagparamsortorderasc) | **[Client](Client.md)** | Set the sort direction (default: ascending)
[**getDealerDetailsRequest**](#markdown-header-getdealerdetailsrequest) | **[DealerDetailsRequest](../Model/DealerDetailsRequest.md)** | Returns a DealerDetailsRequest Object
[**getVehicleTagSearch**](#markdown-header-getvehicletagsearch) | **[TagSearch](../Model/TagSearch.md)** | Returns a TagSearch Object
[**getVehicleSearch**](#markdown-header-getvehiclesearch) | **[VehicleSearch](../Model/VehicleSearch.md)** | Returns a VehicleSearch Object
[**getVehicleDetailsRequest**](#markdown-header-getvehicledetailsrequest) | **[VehicleDetailsRequest](../Model/VehicleDetailsRequest.md)** | Returns a VehicleDetailsRequest Object
[**getDealerDetails**](#markdown-header-getdealerdetails) | **[DealerResponse](../Model/DealerResponse.md)** | Returns a DealerResponse Object with data from API
[**getVehicleAggregationTags**](#markdown-header-getvehicleaggregationtags) | **[TagSearchResult](../Model/TagSearchResult.md)** | Returns a TagSearchResult Object
[**getVehicleSearchResult**](#markdown-header-getvehiclesearchresult) | **[VehicleSearchResult](../Model/VehicleSearchResult.md)** | Returns a VehicleSearchResult Object
[**getVehicleDetails**](#markdown-header-getvehicledetails) | **[VehicleResponse](../Model/VehicleResponse.md)** | Returns a VehicleResponse Object
[**getLastMetadata**](#markdown-header-getlastmetadata) | **[MetaResponse](../Model/MetaResponse.md)** | Returns a MetaResponse Object from last request


### Constants for vehicle aggregation types

Constant | Type | Value
------------- | ------------- | -------------
**AGG_TYPE_MANUFACTURER** | **String** | MANUFACTURER
**AGG_TYPE_SERIES** | **String** | SERIES
**AGG_TYPE_ORDERSTATUS** | **String** | ORDERSTATUS
**AGG_TYPE_BODYTYPE** | **String** | BODYTYPE
**AGG_TYPE_BODYTYPEGROUP** | **String** | BODYTYPEGROUP
**AGG_TYPE_FUELTYPEPRIMARY** | **String** | FUELTYPEPRIMARY
**AGG_TYPE_DEALERID_VEHICLEOWNER** | **String** | DEALERID_VEHICLEOWNER
**AGG_TYPE_COLORTAG** | **String** | COLORTAG
**AGG_TYPE_UPHOLSTERYTYPETAG** | **String** | UPHOLSTERYTYPETAG
**AGG_TYPE_PROPULSIONTYPETAG** | **String** | PROPULSIONTYPETAG
**AGG_TYPE_POLLUTIONBADGETAG** | **String** | POLLUTIONBADGETAG
**AGG_TYPE_EURONORMTAG** | **String** | EURONORMTAG
**AGG_TYPE_TRANSMISSIONTYPE** | **String** | TRANSMISSIONTYPE


# **getClientVersion**
> string getClientVersion()

Returns the version of the API Client

### Return type
string


# **getHost**
> string getHost()

Returns the API Url

### Return type
string


# **setHost**
> Client setHost(string $host)

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 host | string | set the API Url | overwrites the host from configuration

### Return type
[Client](Client.md)


# **getDealerId**
> int getDealerId()

Returns the Dealer ID

### Return type
int


# **setDealerId**
> Client setDealerId(int $dealerId)

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 dealerId | int | set the Dealer ID | overwrites the Dealer ID from configuration

### Return type
[Client](Client.md)


# **getPagParams**
> array getPagParams()

Returns the Pagination Parameters

### Return type
array

Default values:
```php
['result_count' => 10, 'result_offset' => 0, 'sort_type' => 'id', 'sort_order_asc' => true]
```


# **setPaginationParameters**
> Client setPaginationParameters(array $pagParams)

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 pagParams | array | set the Pagination Parameters Object | structure is like [getPagParams](Client.md#getpagparams)

### Return type
[Client](Client.md)


# **getPaginationParameters**
> Swagger\Client\Model\PaginationParameters getPaginationParameters()

Returns the Pagination Parameters Object

### Return type
[Swagger\Client\Model\PaginationParameters](../Model/PaginationParameters.md)


# **setPagParamResultCount**
> Client setPagParamResultCount(int $cnt)

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 cnt | int | Set the number of return values | value in PaginationParameters

### Return type
[Client](Client.md)



# **setPagParamResultOffset**
> Client setPagParamResultOffset(int $offset)

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 offset | int | Set the offset of return values | value in PaginationParameters

### Return type
[Client](Client.md)


# **setPagParamSortType**
> Client setPagParamSortType(string $sortType)

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 sortType | string | Set the fieldname for sort (default: id) | value in PaginationParameters

### Return type
[Client](Client.md)


# **setPagParamSortOrderAsc**
> Client setPagParamSortOrderAsc(bool $sortOrderAsc)

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 sortOrderAsc | bool | Set the sort direction (default: ascending) | value in PaginationParameters

### Return type
[Client](Client.md)


# **getDealerDetailsRequest**
> Swagger\Client\Model\DealerDetailsRequest getDealerDetailsRequest()

Returns a DealerDetailsRequest Object

used for [getDealerDetails()](Client.md#getdealerdetails)

### Return type
[Swagger\Client\Model\DealerDetailsRequest](../Model/DealerDetailsRequest.md)


# **getVehicleTagSearch**
> Swagger\Client\Model\TagSearch getVehicleTagSearch()

Returns a TagSearch Object

used for [getVehicleAggregationTags()](Client.md#getvehicleaggregationtags)

### Return type
[Swagger\Client\Model\TagSearch](../Model/TagSearch.md)


# **getVehicleSearch**
> Swagger\Client\Model\VehicleSearch getVehicleSearch()

Returns a VehicleSearch Object

used for [getVehicleSearchResult()](Client.md#getvehiclesearchresult)

### Return type
[Swagger\Client\Model\VehicleSearch](../Model/VehicleSearch.md)


# **getVehicleDetailsRequest**
> Swagger\Client\Model\VehicleDetailsRequest getVehicleDetailsRequest()

Returns a VehicleDetailsRequest Object

used for [getVehicleDetails()](Client.md#getvehicledetails)

### Return type
[Swagger\Client\Model\VehicleDetailsRequest](../Model/VehicleDetailsRequest.md)


# **getDealerDetails**
> Swagger\Client\Model\DealerResponse getDealerDetails(DealerDetailsRequest $dealerDetailsRequest)

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 dealerDetailsRequest | [DealerDetailsRequest](../Model/DealerDetailsRequest.md) | Returns a DealerResponse Object with data from API | 

### Return type
[Swagger\Client\Model\DealerResponse](../Model/DealerResponse.md)


# **getVehicleAggregationTags**
> Swagger\Client\Model\TagSearchResult getVehicleAggregationTags(TagSearch $vehicleTagSearch, int $dealerid = null)

Returns a TagSearchResult Object with data from API

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 vehicleTagSearch | [TagSearch](../Model/TagSearch.md) |  | **required**
 dealerid | int | default *null* (Dealer ID from configuration will be used) | *optional*

### Return type
[Swagger\Client\Model\TagSearchResult](../Model/TagSearchResult.md)


# **getVehicleSearchResult**
> Swagger\Client\Model\VehicleSearchResult getVehicleSearchResult(VehicleSearch $vehicleSearch, int $dealerid = null)

Returns a VehicleSearchResult Object with data from API

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 vehicleSearch | [VehicleSearch](../Model/VehicleSearch.md) |  | **required**
 dealerid | int | default *null* (Dealer ID from configuration will be used) | *optional*

### Return type
[Swagger\Client\Model\VehicleSearchResult](../Model/VehicleSearchResult.md)


# **getVehicleDetails**
> Swagger\Client\Model\VehicleResponse getVehicleDetails(VehicleDetailsRequest $vehicleDetailsRequest, int $dealerid = null)

Returns a VehicleResponse Object with data from API

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 vehicleDetailsRequest | [VehicleDetailsRequest](../Model/VehicleDetailsRequest.md) |  | **required**
 dealerid | int | default *null* (Dealer ID from configuration will be used) | *optional*

### Return type
[Swagger\Client\Model\VehicleResponse](../Model/VehicleResponse.md)


# **getLastMetadata**
> Swagger\Client\Model\MetaResponse getLastMetadata()

Returns a MetaResponse Object with data from last request

### Return type
[Swagger\Client\Model\MetaResponse](../Model/MetaResponse.md)
